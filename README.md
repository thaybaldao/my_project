This project emulates a dice. 

It has two methods:
- initializeSeed(): creates a seed for the random function used in rollDice().
- rollDice(faces): generates a random number from 1 to faces, as if it were a non-addicted dice.